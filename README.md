#  Cet été, j'ai repris mon vélo

Ce projet est destiné à produire un document LaTeX.

## Le document produit raconte le voyage à vélo après trois ans sans vélo

Le second voyage que j'ai raconté. 
J'ai fait un petit trajet pour me remettre au vélo. 
En gros, j'ai beaucoup diminué le vélo en arrivant sur Paris. 
Puis, pendant 3 ans, j'ai complètement arrêté le vélo.
Donc, quand j'ai décidé de reprendre le vélo, je devais essayer de faire un petit trajet pour savoir si je pouvais rouler de nouveau.
En septembre 2005, j'ai fait un voyage de 2100 km en 11,5 jours.
Je suis parti de Paris, allé dans l'est de la France, puis dans le centre avant de rentrer à Paris. 
Là, j'avais définitivement adopté l'hôtel.
Pas tant pour le confort du lit que pour la douche et le petit déjeuner chaud.
Je ne sais pas vraiment pourquoi j'ai raconté ce trajet plus que d'autres.
La distance était plutôt faible et je n'ai même pas été dans un endroit particulièrement inhabituel.
Mais bon, comme c'est écrit, je le laisse avec les autres.

## Lecture du document compilé
Le document compilé est disponible ici : [retourvelo.pdf](https://scarpet42.gitlab.io/retourvelo/retourvelo.pdf)

## License

La licence est la WTFPL : [WTFPL](http://www.wtfpl.net/)
C'est la plus permissive des licences.
Si je ne le fais pas, vous n'avez rien le droit de faire du texte sans mon accord.
